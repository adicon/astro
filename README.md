# Astro


## Repositori per progetti di astronomia


### Contiene dati e codice per lavorare sugli stessi.

In `SN2023vyl_data_OASDG` ci sono le immagini per il progetto con l'Università della Calabria. I dati vengono importati in un notebook su Google Colab per essere elaborati con le apposite routine Python.

Progetto a cura di Andrea Di Dato e Luca Izzo.


## License
Licenza Creative Commons **CC BY-NC** (Attribuzione – Non Commerciale)

https://creativecommons.org/licenses/by-nc/4.0/deed.it




